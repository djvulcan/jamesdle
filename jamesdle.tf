locals {
  domain_root   = data.terraform_remote_state.core.outputs.domain_root
  jamesdle_fqdn = "${var.jamesdle_bucket_website_address}.${local.domain_root}"
}

resource "aws_s3_bucket" "jamesdle" {
  bucket        = local.jamesdle_fqdn
  force_destroy = true
}

resource "aws_s3_bucket_website_configuration" "jamesdle" {
  bucket = aws_s3_bucket.jamesdle.bucket

  index_document {
    suffix = "index.html"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "jamesdle" {
  bucket = aws_s3_bucket.jamesdle.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "jamesdle" {
  bucket = aws_s3_bucket.jamesdle.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "read_only" {
  bucket = aws_s3_bucket.jamesdle.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_ownership_controls" "s3_bucket_acl_ownership" {
  bucket = aws_s3_bucket.jamesdle.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
  depends_on = [aws_s3_bucket_public_access_block.read_only]
}

resource "aws_s3_bucket_acl" "public" {
  bucket     = aws_s3_bucket.jamesdle.id
  acl        = "public-read"
  depends_on = [aws_s3_bucket_ownership_controls.s3_bucket_acl_ownership]
}

resource "aws_s3_bucket_policy" "jamesdle_public_read" {
  bucket = aws_s3_bucket.jamesdle.id

  policy     = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "${aws_s3_bucket.jamesdle.arn}/*"
        }
    ]
}
POLICY
  depends_on = [aws_s3_bucket_public_access_block.read_only]
}

resource "aws_route53_record" "jamesdle" {
  zone_id = data.terraform_remote_state.core.outputs.hosted_zone_id
  name    = local.jamesdle_fqdn
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.jamesdle.domain_name
    zone_id                = aws_cloudfront_distribution.jamesdle.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_cloudfront_origin_access_identity" "jamesdle" {
  provider = aws.usa
  comment  = "Jamesdle"
}

resource "aws_cloudfront_distribution" "jamesdle" {
  provider = aws.usa
  origin {
    domain_name = aws_s3_bucket.jamesdle.bucket_regional_domain_name
    origin_id   = aws_cloudfront_origin_access_identity.jamesdle.id
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.jamesdle.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Some comment"
  default_root_object = "index.html"

  aliases = [local.jamesdle_fqdn]

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_cloudfront_origin_access_identity.jamesdle.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "blacklist"
      locations        = ["RU"]
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.jamesdle.arn
    ssl_support_method  = "sni-only"
  }
}

resource "aws_acm_certificate" "jamesdle" {
  provider          = aws.usa
  domain_name       = local.jamesdle_fqdn
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation" {
  for_each = {
    for dvo in aws_acm_certificate.jamesdle.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.terraform_remote_state.core.outputs.hosted_zone_id
}

locals {
  domain_validation = aws_acm_certificate.jamesdle.domain_validation_options
}

resource "aws_acm_certificate_validation" "jamesdle" {
  provider                = aws.usa
  certificate_arn         = aws_acm_certificate.jamesdle.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation : record.fqdn]
}

output "dns" {
  value = aws_cloudfront_distribution.jamesdle.domain_name
}