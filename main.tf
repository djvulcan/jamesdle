provider "aws" {
  region = var.aws_region
}

provider "aws" {
  region = "us-east-1"
  alias  = "usa"
}

terraform {
  required_version = "~> 1.6"
  backend "s3" {
    bucket  = "jimbo-london-tfstate"
    key     = "core-infra/jamesdle.tfstate"
    encrypt = true
    region  = "eu-west-2"
  }
}
