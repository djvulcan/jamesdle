data "terraform_remote_state" "core" {
  backend = "s3"
  config = {
    bucket  = "jimbo-london-tfstate"
    key     = "core-infra/terraform.tfstate"
    encrypt = true
    region  = "eu-west-2"
  }
}