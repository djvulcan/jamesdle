import { useEffect } from 'react';
import './App.css';
import GuessGrid, { AlertContainer } from './GuessGrid';
import Keyboard from './Keyboard';
import Modal from './Modal';
import { ComputeGuess, Validate, getDailyWord, getDateIndex } from './WordBank';

export const GUESSES = 6
export const WORD_LENGTH = 5
let solution = ""
let evaluations = []
let guesses = []
const FLIP_DURATION_TIMEOUT = 500
const DANCE_DURATION = 500
let nightMode = false
let gameState = "playing"
function App() {
  useEffect(() => {
    updateGame()
  })
  newGuess()
  return (
    <div className="App">
      <header className='header'>
      <div className='menu-left' style={{width: "10rem"}}>
      </div>
      <div className='title'>Jamesdle</div>
      <div className='menu-right' style={{width: "10rem", textAlign: "right"}}>
      <input type="checkbox" className='checkbox' value = "nightmode" name="nightmode" onChange={e => setNightMode(e)}></input> Dark Mode
      </div>
      </header>
      <main>
        <div className='game'>
          <AlertContainer />
          <GuessGrid />
          <Keyboard />
          <Modal />
        </div>
      </main>
    </div>
  );
}

export default App;

function setNightMode(e) {
  e.target.checked ? document.body.classList.add("nightmode") : document.body.classList.remove("nightmode")
  localStorage.setItem("nightmode",e.target.checked);
  nightMode = e.target.checked
}

function getFromState() {
  const jamesdleState = JSON.parse(localStorage.getItem("jamesdle"))
  nightMode = localStorage.getItem("nightmode") || "false"
  if (jamesdleState) {
    evaluations = jamesdleState.evaluations
    guesses = jamesdleState.boardState || []
    solution = jamesdleState.solution
    if (solution !== getDailyWord()) {
      resetGame()
    }
  } else {
    solution = getDailyWord()
  }
}

function saveState() {
  const jamesdleState = {"evaluations": evaluations, "boardState": guesses, "solution": solution}
  localStorage.setItem("nightmode", nightMode)
  localStorage.setItem("jamesdle", JSON.stringify(jamesdleState))
}

function updateGame() {
  getFromState()
  nightMode && nightMode === "true" ? document.body.classList.add("nightmode"): document.body.classList.remove("nightmode")
  document.querySelectorAll("input[value=nightmode]")[0].checked = nightMode === "true"
  let guessGrid = document.querySelector("[data-guess-grid]")
  const keyboard = document.querySelector("[data-keyboard]")
  guesses.forEach((guess, index) => {
    const guessArray = guess.split('')
    guessGrid = document.querySelector("[data-guess-grid]")
    const evaluation = evaluations[index]
    guessArray.forEach((letter, index) => {
      const nextTile = guessGrid.querySelector(":not([data-letter])")
      nextTile.dataset.letter = letter.toLowerCase()
      nextTile.textContent = letter
      nextTile.dataset.state = evaluation[index]
      const key = keyboard.querySelector(`[data-key="${letter}"i]`)
      if (key.classList.contains("present")) {
        key.classList.remove("absent")
      }
      key.classList.add(evaluation[index])
    })
    const activeTiles = [...guessGrid.querySelectorAll('[data-state="active"]')]
    CheckWinLose(guess, activeTiles)
  })
}

function newGuess() {
  startInput()
}

function resetGame() {
  const guessGrid = document.querySelector("[data-guess-grid]")
  const usedTiles = [...guessGrid.querySelectorAll('[data-letter]')]
  usedTiles.forEach((...params) => resetTile(...params))
  const keys = [...document.querySelectorAll("[data-key]")]
  keys.forEach((...params) => resetKey(...params))
  hideOverlay()
  evaluations = []
  guesses = []
  solution = getDailyWord()
  saveState()
  gameState = "playing"
}

function resetTile(tile) {
  tile.textContent = ""
  delete tile.dataset.state
  delete tile.dataset.letter
}

function resetKey(key) {
  key.classList.remove("present")
  key.classList.remove("absent")
  key.classList.remove("correct")
}

function startInput() {
  document.addEventListener("click", handleMouseClick)
  document.addEventListener("keydown", handleKeyPress)
}

function stopInput() {
  document.removeEventListener("click", handleMouseClick)
  document.removeEventListener("keydown", handleKeyPress)
}

function handleMouseClick(e) {
  if (gameState === "playing") {
    if (e.target.matches("[data-key]")) {
      pressKey(e.target.dataset.key)
      return
    }
    if (e.target.matches("[data-enter]")) {
      submitGuess()
      return
    }
    if (e.target.matches("[data-delete]")) {
      deleteKey()
      return
    }
  }
  if (e.target.id === 'share-button') {
    doShare(getTranscript())
    return
  }
  if (e.target.id === 'reset-button') {
    resetGame()
    return
  }
  if (e.target.id === 'close-icon') {
    hideOverlay()
    return
  }
}

function doShare(text) {
  if (navigator.canShare && navigator.canShare({ text: text })) {
    navigator.share({
      text: text
    })
    .then(() => console.log('Share was successful.'))
    .catch((error) => console.log('Sharing failed', error));
    navigator.clipboard.writeText(text).then(()=> showAlert("Copied to Clipboard"))
  } else {
    console.log(`Your system doesn't support sharing files.`);
    navigator.clipboard.writeText(text).then(()=> showAlert("Copied to Clipboard"))
  }
  return
}

function handleKeyPress(e) {
  if (gameState === "playing") {
    if (e.key === "Enter") {
      submitGuess()
      return
    }
    if (e.key === "Backspace" || e.key === "Delete") {
      deleteKey()
      return
    }
    if (e.key.match(/^[a-z]$/)) {
      pressKey(e.key)
      return
    }
  }
}

function pressKey(key){
  const guessGrid = document.querySelector("[data-guess-grid]")
  const activeTiles = getActiveTiles(guessGrid)
  if (activeTiles.length >= WORD_LENGTH) return
  const nextTile = guessGrid.querySelector(":not([data-letter])")
  nextTile.dataset.letter = key.toLowerCase()
  nextTile.textContent = key
  nextTile.dataset.state = "active"
}

function getActiveTiles(guessGrid) {
  return(guessGrid.querySelectorAll('[data-state="active"]'))
}
function submitGuess(stateFul = true){
  const guessGrid = document.querySelector("[data-guess-grid]")
  const activeTiles = [...getActiveTiles(guessGrid)]
  const guess = activeTiles.reduce((word, tile) => {
    return word + tile.dataset.letter
  }, "")
  if (activeTiles.length !== WORD_LENGTH) {
    showAlert("Not enough letters")
    shakeTiles(activeTiles)
    return
  }

  if (!Validate(guess)) {
    showAlert("Not a valid word!")
    shakeTiles(activeTiles)
    return
  }
  stopInput()
  const evaluation = ComputeGuess(guess, solution)
  if (stateFul) {
    guesses.push(guess)
    evaluations.push(evaluation)
    saveState()
  }
  activeTiles.forEach((...params) => flipTiles(...params,guess,evaluation))
  
}
function deleteKey(){
  const guessGrid = document.querySelector("[data-guess-grid]")
  const activeTiles = getActiveTiles(guessGrid)
  const lastTile = activeTiles[activeTiles.length -1]
  if (lastTile == null) return
  lastTile.textContent = ""
  delete lastTile.dataset.state
  delete lastTile.dataset.letter
}

function showAlert(message, duration=1000) {
  const alertContainer = document.querySelector("[data-alert-container]")
  const alert = document.createElement("div")
  alert.textContent = message
  alert.classList.add("alert")
  alertContainer.prepend(alert)
  if (duration == null) return
  setTimeout(() => {
    alert.classList.add("hide")
    alert.addEventListener("transitionend", () => {
      alert.remove()
    })
  }, duration)
  return Promise.resolve()
}

function shakeTiles(tiles) {
  tiles.forEach(tile => {
    tile.classList.add("shake")
    tile.addEventListener("animationend", () => {
      tile.classList.remove("shake")
    }, {once: true})
  });
}

function flipTiles(tile, index, array, guess, evaluation) {
  const keyboard = document.querySelector("[data-keyboard]")
  const letter = tile.dataset.letter
  const key = keyboard.querySelector(`[data-key="${letter}"i]`)
  setTimeout(() => [
    tile.classList.add("flip")
  ], index * FLIP_DURATION_TIMEOUT / 2)
  tile.addEventListener("transitionend", () => {
    tile.classList.remove("flip")
    tile.dataset.state = evaluation[index]
    if (key.classList.contains("present")) {
      key.classList.remove("absent")
    }
    key.classList.add(evaluation[index])
    if (index === array.length -1) {
      tile.addEventListener("transitionend", () => {
        startInput()
        CheckWinLose(guess, array)
      },
      { once: true})
    }
  },
  { once: true})
}

function CheckWinLose(guess, tiles) {
  if (guess === solution) {
    const skills = ["Genius", "Magnificent", "Impressive", "Splendid", "Great", "Phew"]
    showAlert(skills[evaluations.length-1], 1000).then(() => {
      danceTiles(tiles)
      gameState = "won"
      setTimeout(() => {
        showOverlay()
      },1500)
    })
    
  }
  const guessGrid = document.querySelector("[data-guess-grid]")
  const remainingTiles = guessGrid.querySelectorAll(":not([data-letter])")
  if (remainingTiles.length === 0 && guess !== solution) {
    showAlert("You lose!!! It was: " + solution.toUpperCase(), 2000)
    gameState = "lost"
    setTimeout(() => {
      showOverlay()
    },1500)
  }
}

function getTranscript () {
  let transcript = "Jamesdle " + getDateIndex() + " " + evaluations.length + "/6\n\n"
  evaluations.forEach((evaluation) =>{
    evaluation.forEach((letter) =>{
      if (letter === "absent") {
        transcript += nightMode === "true" ? "⬛" : "⬜"
        return
      }
      if (letter === "present") {
        transcript += "🟨"
        return
      }
      if (letter === "correct") {
        transcript += "🟩"
        return
      }
    })
    transcript += "\n"
  })
  return(transcript)
}

function showOverlay() {
  const overlay = document.querySelector(".overlay")
  overlay.classList.add("open")
}

function hideOverlay() {
  const overlay = document.querySelector(".overlay")
  overlay.classList.remove("open")
}

function danceTiles(tiles) {
  tiles.forEach((tile, index) => {
    setTimeout(() => {
      tile.classList.add("dance")
      tile.addEventListener("animationend", () => {
        tile.classList.remove("dance")
      }, {once: true})
    }, index * DANCE_DURATION / 5)
  })
}
