function Keyboard() {
    const keyboardLetters1 = ['Q','W','E','R','T','Y','U','I','O','P']
    const keyboardLetters2 = ['A','S','D','F','G','H','J','K','L']
    const keyboardLetters3 = ['Z','X','C','V','B','N','M']
    const space = <div className="space" key="40"></div>
    const space2 = <div className="space" key="41"></div>
    const enter = <button data-enter className="key large" key="50">Enter</button>
    const backspace = <button data-delete className="key large" key="60"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path fill="var(--color-tone-1)" d="M22 3H7c-.69 0-1.23.35-1.59.88L0 12l5.41 8.11c.36.53.9.89 1.59.89h15c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H7.07L2.4 12l4.66-7H22v14zm-11.59-2L14 13.41 17.59 17 19 15.59 15.41 12 19 8.41 17.59 7 14 10.59 10.41 7 9 8.41 12.59 12 9 15.59z"></path></svg></button>
    const keyboard1 = keyboardLetters1.map((item,key) => {
        return(<button className="key" data-key={item} key={key}>{item}</button>)
    })
    const keyboard2 = keyboardLetters2.map((item,key) => {
        return(<button className="key" data-key={item} key={key+10}>{item}</button>)
    })
    const keyboard3 = keyboardLetters3.map((item,key) => {
        return(<button className="key" data-key={item} key={key+19}>{item}</button>)
    })
    const keyboard = [...keyboard1,space,...keyboard2,space2,enter,...keyboard3,backspace]
    return (
      <div data-keyboard className="keyboard">
        {keyboard}
      </div>
    );
}

export default Keyboard;