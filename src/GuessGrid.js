import { GUESSES, WORD_LENGTH } from "./App"

function GuessGrid() {
    const tile = [...Array(WORD_LENGTH * GUESSES)].map((e, i) => <div className="tile" key={i}></div>)
    return (
        <div className="board-container">
            <div data-guess-grid className="guess-grid">
            {tile}
            </div>
        </div>
    )
}

export default GuessGrid

export function AlertContainer() {
    return (
        <div className="alert-container" data-alert-container ></div>
    )
}