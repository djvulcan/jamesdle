import wordBanks from './word-bank.json';

const wordBank = wordBanks.wordbank
const dictionary = wordBanks.dictionary
export function TestWord() {
    return ("crane")
}

export function Validate(word) {
    return (wordBank.includes(word) || dictionary.includes(word))
}

export function getRandomWord() {
    const randomIndex = Math.floor(Math.random() * wordBank.length);
    return wordBank[randomIndex];
}

export function getDateIndex() {
    let date_1 = new Date('06/20/2021');
    let date_2 = new Date();
    let difference = date_2.getTime() - date_1.getTime();
    let TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
    return TotalDays;
}

export function getDailyWord() {
    return wordBank[getDateIndex()];
}


export function ComputeGuess(guess, solution) {
    const result = [];
    const guessArray = guess.split('')
    const answerArray = solution.split('')
    const answerLetterCount = {};

    if (guess.length !== solution.length) {
        return result
    }

    guessArray.forEach((letter, index) => {
        const currentAnswerLetter = answerArray[index]
        answerLetterCount[currentAnswerLetter] = answerLetterCount[currentAnswerLetter] ? answerLetterCount[currentAnswerLetter] +1 : 1

        if (currentAnswerLetter === letter) {
            result.push('correct')
        } else if (answerArray.includes(letter)) {
            result.push('present')
        } else {
            result.push('absent')
        }
    })
    result.forEach((curResult, resultIndex) => {
        if (curResult !== 'present') {
            return
        }

        const guessLetter = guessArray[resultIndex]

        answerArray.forEach((currentAnswerLetter, answerIndex) =>{
            if (currentAnswerLetter !== guessLetter) {
                return
            }
            if (result[answerIndex] === 'correct') {
                result[resultIndex] = 'absent'
            }
            if (answerLetterCount[guessLetter] <= 0) {
                result[resultIndex] = 'absent'
            }
        })
        answerLetterCount[guessLetter]--
    })

    return(result)
}