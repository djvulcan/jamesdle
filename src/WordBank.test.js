import { ComputeGuess } from "./WordBank"

test('returns empty array when given incomplete guess', ()=> {
    expect(ComputeGuess('so', 'basic')).toEqual([])
})

test('works with match and present', () => {
    expect(ComputeGuess('boost', 'basic')).toEqual([
        'correct',
        'absent',
        'absent',
        'present',
        'absent'
    ])
})

test('works with full match', () => {
    expect(ComputeGuess('boost', 'boost')).toEqual([
        'correct',
        'correct',
        'correct',
        'correct',
        'correct'
    ])
})

test('works with full miss', () => {
    expect(ComputeGuess('boost', 'crane')).toEqual([
        'absent',
        'absent',
        'absent',
        'absent',
        'absent'
    ])
})

test('only does one match when 2 letters are present', () => {
    expect(ComputeGuess('solid', 'boost')).toEqual([
        'present',
        'correct',
        'absent',
        'absent',
        'absent'
    ])
})

test('when 2 letters are present but answer only has 1 of the letters', () => {
    expect(ComputeGuess('allol', 'smelt')).toEqual([
        'absent',
        'present',
        'absent',
        'absent',
        'absent'
    ])
})

test('when 1 letter matches but guess has more of the same letter', () => {
    expect(ComputeGuess('allol', 'colon')).toEqual([
        'absent',
        'absent',
        'correct',
        'correct',
        'absent'
    ])
})